import remi.gui as gui
from remi import start, App
import TwitterFunctions


class MyApp(App):
    def __init__(self, *args):
        super(MyApp, self).__init__(*args)

    def main(self):

        self.container_main = gui.VBox(width='100%', height='100%')
        self.container_result = gui.Container()
        container_options = gui.Container(width=200, layout_orientation=gui.Container.LAYOUT_HORIZONTAL)

        label_title = gui.Label('Fake News and Super Spreader Detection on Twitter:', style={'font-size': '30px', 'font-weight': 'bold'})
        label_description = gui.Label('Enter Text, URL or Account Name in English:', style={'font-size': '20px'})

        option = gui.DropDownItem('Please select input type:')
        option1 = gui.DropDownItem('Text')
        option2 = gui.DropDownItem('URL')
        option3 = gui.DropDownItem('User')
        self.option_list = gui.DropDown([option, option1, option2, option3])

        self.textInput = gui.TextInput(width=500, height=200)
        self.bt = gui.Button('PREDICT')


        self.bt.onclick.do(self.on_button_pressed_text)

        container_options.append([self.option_list])
        self.container_main.append([label_title, label_description, container_options, self.textInput, self.bt, self.container_result])
        # self.container_main.append(self.bt)

        return self.container_main


    # Click function
    def on_button_pressed_text(self, widget):

        option = self.option_list.get_value()
        self.container_result.empty()

        # Detect Text
        if option == 'Text':
            trueTextLabel = gui.Label('This Tweet is True', style={'color': 'green'})
            falseTextLabel = gui.Label('This Tweet is Fake', style={'color': 'red'})
            result = TwitterFunctions.detectTweet_Text(self.textInput.get_text())
            if result:
                self.container_result.append(trueTextLabel)
            else:
                self.container_result.append(falseTextLabel)

        # Detect URL
        if option == 'URL':
            trueURLLabel = gui.Label('This Tweet is True', style={'color': 'green'})
            falseURLLabel = gui.Label('This Tweet is Fake', style={'color': 'red'})
            result = TwitterFunctions.detectTweet_URL(self.textInput.get_text())
            if result:
                self.container_result.append(trueURLLabel)
            else:
                self.container_result.append(falseURLLabel)

        # Detect User
        if option == 'User':
            trueUserLabel = gui.Label('This User is a Super Spreader of Fake News', style={'color': 'red'})
            falseUserLabel = gui.Label('This User is not a Super Spreader of Fake News', style={'color': 'green'})
            result = TwitterFunctions.detectUser(self.textInput.get_text())
            if result:
                self.container_result.append(trueUserLabel)
            else:
                self.container_result.append(falseUserLabel)

        print(result)


# Launch Server
start(MyApp, port=8083)