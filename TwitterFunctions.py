import string
import re
import nltk

nltk.download('stopwords')
nltk.download('punkt')
from nltk.tokenize import word_tokenize
from nltk.corpus import stopwords

import pandas as pd

from keras.models import load_model
from keras.preprocessing import text, sequence
from keras.models import Sequential



def cleanText(text):
    # Creating dictionary of common contractions.
    dic_contraction = {"ain't": "is not", "aren't": "are not", "can't": "cannot", "'cause": "because",
                       "could've": "could have", "couldn't": "could not", "didn't": "did not", "doesn't": "does not",
                       "don't": "do not", "hadn't": "had not", "hasn't": "has not", "haven't": "have not",
                       "he'd": "he would", "he'll": "he will", "he's": "he is", "how'd": "how did",
                       "how'd'y": "how do you", "how'll": "how will", "how's": "how is", "I'd": "I would",
                       "I'd've": "I would have", "I'll": "I will", "I'll've": "I will have", "I'm": "I am",
                       "I've": "I have", "i'd": "i would", "i'd've": "i would have", "i'll": "i will",
                       "i'll've": "i will have", "i'm": "i am", "i've": "i have", "isn't": "is not", "it'd": "it would",
                       "it'd've": "it would have", "it'll": "it will", "it'll've": "it will have", "it's": "it is",
                       "let's": "let us", "ma'am": "madam", "mayn't": "may not", "might've": "might have",
                       "mightn't": "might not", "mightn't've": "might not have", "must've": "must have",
                       "mustn't": "must not", "mustn't've": "must not have", "needn't": "need not",
                       "needn't've": "need not have", "o'clock": "of the clock", "oughtn't": "ought not",
                       "oughtn't've": "ought not have", "shan't": "shall not", "sha'n't": "shall not",
                       "shan't've": "shall not have", "she'd": "she would", "she'd've": "she would have",
                       "she'll": "she will", "she'll've": "she will have", "she's": "she is",
                       "should've": "should have", "shouldn't": "should not", "shouldn't've": "should not have",
                       "so've": "so have", "so's": "so as", "this's": "this is", "that'd": "that would",
                       "that'd've": "that would have", "that's": "that is", "there'd": "there would",
                       "there'd've": "there would have", "there's": "there is", "here's": "here is",
                       "they'd": "they would", "they'd've": "they would have", "they'll": "they will",
                       "they'll've": "they will have", "they're": "they are", "they've": "they have",
                       "to've": "to have", "wasn't": "was not", "we'd": "we would", "we'd've": "we would have",
                       "we'll": "we will", "we'll've": "we will have", "we're": "we are", "we've": "we have",
                       "weren't": "were not", "what'll": "what will", "what'll've": "what will have",
                       "what're": "what are", "what's": "what is", "what've": "what have", "when's": "when is",
                       "when've": "when have", "where'd": "where did", "where's": "where is", "where've": "where have",
                       "who'll": "who will", "who'll've": "who will have", "who's": "who is", "who've": "who have",
                       "why's": "why is", "why've": "why have", "will've": "will have", "won't": "will not",
                       "won't've": "will not have", "would've": "would have", "wouldn't": "would not",
                       "wouldn't've": "would not have", "y'all": "you all", "y'all'd": "you all would",
                       "y'all'd've": "you all would have", "y'all're": "you all are", "y'all've": "you all have",
                       "you'd": "you would", "you'd've": "you would have", "you'll": "you will",
                       "you'll've": "you will have", "you're": "you are", "you've": "you have"}

    # Function to collect contractions in data
    def get_contractions(dic_contraction):
        contraction_re = re.compile('(%s)' % '|'.join(dic_contraction.keys()))
        return dic_contraction, contraction_re

    # Function to replace contractions
    def replace_contractions(text):
        contractions, contractions_re = get_contractions(dic_contraction)

        def replace(match):
            return contractions[match.group(0)]

        return contractions_re.sub(replace, text)

    # Replacing the contractions
    text = replace_contractions(text)

    # Removing punctuation by matching punctuation in data to string.punctuation list
    text = "".join([char for char in text if char not in string.punctuation])
    text = re.sub('[0-9]+', '', text)

    # Splitting into words
    words = word_tokenize(text)

    # Removing stopwords
    stop_words = set(stopwords.words('english'))
    words = [w for w in words if not w in stop_words]

    # Removing leftover punctuations
    words = [word for word in words if word.isalpha()]

    cleaned_text = ' '.join(words)
    return cleaned_text


tokenizer = text.Tokenizer(num_words=10000)

from twython import Twython

model = load_model('MODEL_LSTM_300.h5')

APP_KEY = 'RtL8oSZo3zvXlflF9nvKKBVxh'
APP_SECRET = 'rLYCnaJ9C0YBEV7fzNqFS1P0k7vwlwDqO4Ehr70UMGFzhvYvGI'

max_len_word = 280

# link = 'https://twitter.com/KoC_117/status/1519100376666054663'
# text = 'This is a test text for simulating user input...'
# user = '@GOVUK'

# Get Twitter API
twitter = Twython(APP_KEY, APP_SECRET, oauth_version=2)
ACCESS_TOKEN = twitter.obtain_access_token()
twitter = Twython(APP_KEY, access_token=ACCESS_TOKEN)


def detectTweet_URL(test_tweet_link):
    # Get full tweet text by using user ID
    id_start = test_tweet_link.index('status/')
    tweet_id = test_tweet_link[id_start + 7:id_start + 26]
    result = twitter.show_status(id=tweet_id, tweet_mode='extended')
    print(result['full_text'])

    # Standardize input text
    testText = cleanText(result['full_text'])

    tlist = []
    tlist.append(testText)

    tokenizer.fit_on_texts(tlist)
    testSq = tokenizer.texts_to_sequences(tlist)
    testPd = sequence.pad_sequences(testSq, maxlen=max_len_word)

    # Predict text
    prediction = model.predict(testPd)
    print(prediction)
    print('%.2f%%' % (prediction[0] * 100))

    if prediction > 0.5:
        return True
    else:
        return False


def detectTweet_Text(test_text):
    # Standardize input text
    testText = cleanText(test_text)

    tlist = []
    tlist.append(testText)

    tokenizer.fit_on_texts(tlist)
    testSq = tokenizer.texts_to_sequences(tlist)
    testPd = sequence.pad_sequences(testSq, maxlen=max_len_word)

    # Predict text
    prediction = model.predict(testPd)
    print(prediction)

    if prediction > 0.5:
        return True
    else:
        return False


def detectUser(username):
    recent_tweets_number = 20

    # Tweet user information
    userInfo = twitter.show_user(screen_name=username)

    counts_followers = userInfo['followers_count']
    counts_following = userInfo['friends_count']
    counts_tweets = userInfo['statuses_count']

    ratio = counts_following / counts_followers

    # print(userInfo)
    print('Counts of Followers:', counts_followers, '\nCounts of Following:', counts_following, '\nCounts of Tweets:',
          counts_tweets, '\n')
    print('Ratio of following to followers:', ratio)

    # Recent tweets
    timeLine = twitter.get_user_timeline(screen_name=username, count=recent_tweets_number, tweet_mode='extended')

    recent_tweets = []
    for t in timeLine:
        recent_tweets.append(t['full_text'])

    # print(recent_tweets)

    # Standardize input text
    df = pd.DataFrame(recent_tweets, columns=['tweets'])
    df = df['tweets'].apply(lambda text: cleanText(text))

    tokenizer.fit_on_texts(df)
    testSq = tokenizer.texts_to_sequences(df)
    testPd = sequence.pad_sequences(testSq, maxlen=max_len_word)

    # Predict text
    prediction = model.predict(testPd)

    print(prediction)

    trueCount = 0

    for p in prediction:
        if p > 0.5:
            trueCount += 1

    true_rate = '{:.2%}'.format(float(trueCount) / float(len(prediction)))
    print(true_rate)

    # Condition for super spreader
    if (counts_followers > 10000) & (ratio < 0.02) & (trueCount <= (recent_tweets_number / 2)):
        return True
    else:
        return False


# detectTweet_URL(link)
# detectTweet_Text(text)
# detectUser(user)
